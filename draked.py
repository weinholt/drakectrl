#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-
# Copyright © 2021 G. Weinholt
# SPDX-License-Identifier: LGPL-3.0-or-later

import argparse
import contextlib
import glob
import re
import signal
import sys
import time

import hidapi                   # python3-hidapi

def fmt_command(command, payload):
    cmd = [command] + payload
    cmd += [0] * (64 - len(cmd))
    return bytearray(cmd)

class Drake(object):

    VENDOR_ID = 0x0b05
    PRODUCT_ID= 0x18ae
    REPORT_ID = bytes([0xEC])
    INTERFACE_NUM = 0

    def __init__(self):
        self.dev = hidapi.Device(vendor_id=self.VENDOR_ID, product_id=self.PRODUCT_ID)

    def close(self):
        """Release USB resources."""
        self.dev.close()

    def _send(self, data):
        self.dev.send_feature_report(data=data, report_id=self.REPORT_ID)

    def set_speeds(self, cooler, embedded):
        assert 0 <= cooler <= 100
        assert 0 <= embedded <= 100
        self._send(fmt_command(0x2A, [cooler, embedded]))

class Sensor:
    def __init__(self, label):
        for filename in glob.glob("/sys/class/hwmon/hwmon*/*_label"):
            with open(filename) as f:
                if f.readline().strip() == label:
                    self.filename = filename.replace('_label', '_input')
                    break

    def get(self):
        with open(self.filename) as f:
            return float(f.readline().strip()) / 1000

def restore_fans_and_die(fan, speeds):
    fan.set_speeds(*speeds)
    sys.exit(0)

def main():
    parser = argparse.ArgumentParser(description='ROG RYUJIN 360 control')
    parser.add_argument('--speeds',
                        help='set the speeds (e.g. 32,32)')
    args = parser.parse_args()
    if args.speeds is None:
        parser.print_help()

    exit_speeds = tuple(int(x) for x in args.speeds.split(','))

    Tctl = Sensor('Tctl')
    Tccd1 = Sensor('Tccd1')
    Tccd2 = Sensor('Tccd2')

    avgs = [60]
    cpufan_avgs = [50]
    vrmfan_avgs = [40]

    with contextlib.closing(Drake()) as fan:
        signal.signal(signal.SIGINT, lambda *x: restore_fans_and_die(fan, exit_speeds))
        signal.signal(signal.SIGTERM, lambda *x: restore_fans_and_die(fan, exit_speeds))

        while True:
            avgs = [(0.6*Tctl.get()+0.40*max(Tccd1.get(),Tccd2.get()))]+avgs[:7]
            temp = sum(avgs)/len(avgs)

            speeds = (100,100)

            if temp > 75:
                speeds = (95, 80)
            elif temp > 65:
                speeds = (80, 50)
            elif temp > 56:
                speeds = (60, 40)
            elif temp > 52:
                speeds = (45, 30)
            else:
                speeds = (10, 5)

            cpufan_avgs = [speeds[0]]+cpufan_avgs[:5]
            vrmfan_avgs = [speeds[1]]+vrmfan_avgs[:5]

            speeds = (int(sum(cpufan_avgs)/len(cpufan_avgs)), int(sum(vrmfan_avgs)/len(vrmfan_avgs)))

            #print(speeds, temp)
            fan.set_speeds(*speeds)

            time.sleep(2)


if __name__ == '__main__':
    main()
