from setuptools import setup

setup(
    name='drakectrl',
    version='1.0.2',
    scripts=['drakectrl.py', 'draked.py'],
    py_modules=['drakectrl', 'draked'],
    license='LGPL-3.0-or-later',
    install_requires=[
        "hidapi"
    ],
)
