#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-
# Copyright © 2021 G. Weinholt
# SPDX-License-Identifier: LGPL-3.0-or-later

import argparse
import contextlib
import re
import hidapi                   # python3-hidapi

def fmt_command(command, payload):
    cmd = [command] + payload
    cmd += [0] * (64 - len(cmd))
    return bytearray(cmd)

class Drake(object):

    VENDOR_ID = 0x0b05
    PRODUCT_ID= 0x18ae
    REPORT_ID = bytes([0xEC])
    INTERFACE_NUM = 0

    def __init__(self):
        self.dev = hidapi.Device(vendor_id=self.VENDOR_ID, product_id=self.PRODUCT_ID)

    def close(self):
        """Release USB resources."""
        self.dev.close()

    def _send(self, data):
        self.dev.send_feature_report(data=data, report_id=self.REPORT_ID)

    def set_speeds(self, cooler, embedded):
        assert 0 <= cooler <= 100
        assert 0 <= embedded <= 100
        self._send(fmt_command(0x2A, [cooler, embedded]))


def main():
    parser = argparse.ArgumentParser(description='ROG RYUJIN 360 control')
    parser.add_argument('--speeds',
                        help='set the speeds (e.g. 32,32)')
    args = parser.parse_args()
    if args.speeds is None:
        parser.print_help()

    with contextlib.closing(Drake()) as fan:

        if args.speeds is not None:
            fan.set_speeds(*[int(x) for x in args.speeds.split(',')])


if __name__ == '__main__':
    main()
